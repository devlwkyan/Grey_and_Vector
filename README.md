# Atividade de Linguagens de Programação

## Objetivos:
* Crie um programa para permitir que o usuário informe uma ou mais imagens que serão convertidas para escala de cinza. Utilize várias threads para converter as imagens mais rapidamente.
* Crie uma classe Vetor que é "Thread Safe". Utilize bloqueios de leitura e escrita em sua implementação.

### Sobre o projeto:
**O programa de conversão de imagem encontra-se na pasta "GreyScale/src/", e é constituido por:**
- Classe Main -> "GreyScale.java"
- Classe responsável pela conversão da imagem -> "GreyConversor.java"
- Classe responsável pela thread -> "Threading.java"

**A classe Vetor encontra-se no diretório inicial e leva o nome "Vetor.java":**
- Classe Vetor, contém a classe que usa bloqueios de leitura e escrita

*Não foi informado nenhuma exigência de apresentação da classe, ou seja, não foi especificado a necessidade dela está ligada ao programa "GreyScale", nem que fosse necessário a demonstração do seu funcionamento, além da própria implementação, por isso a classe só encontra-se de forma genérica demonstrando seu funcionamento*

### Linguagem
O programa foi desenvolvido em Java

### Autores
Lucas Resende Goes
João Victor Santos Mangueira Tavares

### Licensa
Este programa encontra-se sob licensa da MIT, [LICENSA](LICENSE)
