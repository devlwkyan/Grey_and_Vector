public class Threading extends Thread{
    private String image;
    public Threading(String image){
        this.image = image;
    }
    public void run(){
        try{
            while ( true ){
                GreyConversor gconversor = new GreyConversor(  this.image );
                gconversor.start();
            }
        }catch ( Exception e ){
            e.printStackTrace();
        }

    }
}
