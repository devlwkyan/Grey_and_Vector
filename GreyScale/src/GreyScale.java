import java.util.Scanner;

public class  GreyScale{
    public static void main( String[] args ){
        Scanner sc= new Scanner( System.in );
        try{char colour = (char)27;
            while ( true ) {
                System.out.print( colour + "[91m\nRENAME YOUR IMAGE SO THEY DO NOT HAVE WHITE SPACE, OR THE FILE MIGHT NOT BE READ!" );
                System.out.println( colour + "[97m\nDrag and drog the image, copy and paste, or copy the directory address of the image + name of image and paste, then press Enter. When you finish type EXIT: " );
                String pic = sc.nextLine( );
                if ( !pic.equalsIgnoreCase( "exit" ) ) {
                    Threading td = new Threading( pic );
                    td.start( );
                } else {
                    break;
                }
            }
            System.exit( 0 );
        }catch ( Exception e ){
            e.printStackTrace();
        }

    }
}
