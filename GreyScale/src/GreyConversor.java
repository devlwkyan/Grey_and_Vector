import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class GreyConversor extends Thread{
    public GreyConversor( String image){
        BufferedImage img = null;
        File f;
        try{
            f = new File(image);
            img = ImageIO.read(f);
        }catch ( IOException e ){
            e.printStackTrace();
        }

        int width = img.getWidth();
        int height = img.getHeight();
        for (int y=0; y < height; y++){
            for (int x =0; x < width; x++){
                int p = img.getRGB( x, y );

                int a = (p>>24)&0xff;
                int r = (p>>16)&0xff;
                int g = (p>>8)&0xff;
                int b = p&0xff;

                int avg = (r+g+b)/3;
                p = (a<<24) | (avg<<16) | (avg<<8) | avg;
                img.setRGB( x, y, p );
            }
        }

        try{
            f = new File(image.replace( ' ', '_' )+"grey");
            ImageIO.write( img, "jpg", f );
        }catch ( IOException e ){
            e.printStackTrace();
        }
    }


}


